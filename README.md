
- Install aws cli
- Install kubectl
- Install aws-iam-authenticator
- Install eksctl

- User IAM with permissions: AdministratorAccess
- Amazon CodeCommit Repository
- Amazon ECR Repository
- Role from eks_cicd/create_iam_role.sh
- eksctl create cluster -f eks_cicd/cluster.yaml
- kubectl apply -f eks_cicd/deployment.yaml
- kubectl apply -f eks_cicd/service.yaml
- kubectl apply -f eks_cicd/ingress.yaml

- Amazon CodePipeline
  S3
  
  Env CodeBuild:
  `AWS_DEFAULT_REGION` 
  `AWS_CLUSTER_NAME` 
  `AWS_ACCOUNT_ID`
  `IMAGE_REPO_NAME`
  `IMAGE_TAG` 

  `GOOGLE_CLIENT_SECRET`
  `GOOGLE_CLIENT_ID`
  `DATASOURCE_ACCESS_SERVER_JDBC_URL`
  `DATASOURCE_ACCESS_SERVER_USERNAME`
  `DATASOURCE_ACCESS_SERVER_PASSWORD`
  `DATASOURCE_API_JDBC_URL`
  `DATASOURCE_API_USERNAME`
  `DATASOURCE_API_PASSWORD`

- CodeBuildBasePolicy-access_server-eu-central-1 
  Attach this policy to the new Role(CodeBuildKubectlRole) and detach it from the old role.

  Detach this: codebuild-access_server-service-role
  and Attach this: CodeBuildKubectlRole

  codebuild-access_server-service-role delete

- Developer Tools > CodeBuild > Build projects > access_server > Edit Environment
Change to role/CodeBuildKubectlRole
  
- eksctl create iamidentitymapping --cluster cicd-cluster --arn arn:aws:iam::your_accoun_tid_here:role/CodeBuildKubectlRole --group system:masters --username CodeBuildKubectlRole
  
----

eksctl delete cluster -f eks_cicd/cluster.yaml


