package net.proselyte.access_server.service;

import net.proselyte.access_server.dto.access_server.UserRegistrationDto;
import net.proselyte.access_server.entity.access_server.User;
import net.proselyte.access_server.enumeration.Provider;
import net.proselyte.access_server.exception.UserRegistrationException;
import net.proselyte.access_server.repository.access_server.UserRepository;
import net.proselyte.access_server.security.oauth2.OAuth2UserImpl;
import org.h2.tools.DeleteDbFiles;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@SpringBootTest
@TestPropertySource(properties = {"spring.config.location=classpath:application-test.yml"})
@Sql(scripts = "classpath:clean.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class UserServiceTest {

    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository repository;

    @AfterAll
    static void tearDown() {
        DeleteDbFiles.execute("./src/test/resources/testdb", "testdb", false);
    }

    @Test
    void shouldRegisterOAuth2UserWithSuccess_OAuth2UserImpl() {
        //given
        OAuth2UserImpl oAuth2User = mock(OAuth2UserImpl.class);
        when(oAuth2User.getName()).thenReturn("NameOAuth2");
        when(oAuth2User.getEmail()).thenReturn("userOAuth2@mail.com");
        //when
        boolean result = userService.registerOAuth2(oAuth2User);
        //then
        User userDB = userService.findByEmail("userOAuth2@mail.com");
        assertTrue(result);
        assertEquals("NameOAuth2", userDB.getName());
        assertEquals("userOAuth2@mail.com", userDB.getEmail());
        assertEquals("OAuth2", userDB.getPassword());
        assertEquals("GOOGLE", userDB.getProvider().name());
    }

    @Test
    void shouldNotRegisterOAuth2User_OAuth2UserImplExist() {
        //given
        var user = new User();
        user.setId(1L);
        user.setName("NameOAuth2Test");
        user.setEmail("userOAuth2@mail.com");
        user.setPassword("OAuth2Test");
        user.setProvider(Provider.GOOGLE);
        repository.save(user);
        OAuth2UserImpl oAuth2User = mock(OAuth2UserImpl.class);
        when(oAuth2User.getName()).thenReturn("NameOAuth2");
        when(oAuth2User.getEmail()).thenReturn("userOAuth2@mail.com");
        //when
        boolean result = userService.registerOAuth2(oAuth2User);
        //then
        User userDB = userService.findByEmail("userOAuth2@mail.com");
        assertFalse(result);
    }

    @Test
    void shouldRegisterBasicUserWithSuccess_getUserRegistrationDto() {
        //given
        var userDto = new UserRegistrationDto();
        userDto.setName("NameUserDto");
        userDto.setEmail("userDto@mail.com");
        userDto.setPassword("1234");
        //when
        try {
            userService.registerBasic(userDto);
        } catch (UserRegistrationException e) {
            e.printStackTrace();
        }
        //then
        User userDB = userService.findByEmail("userDto@mail.com");
        assertEquals(userDto.getName(), userDB.getName());
        assertEquals(userDto.getEmail(), userDB.getEmail());
//        assertEquals("$2a$12$3fOeXYv.Z9XfJZhCc6fSn.xkvqKd8qHwdi.MX0kkaY0FPhHaYw98a", userDB.getPassword());
//        assertEquals("$2a$12$Lq0jji9vm6skuU1c9s0TIOoqJjmPCRl2ed/eZpMhxlyu67z6Lihca", userDB.getPassword());
//        assertEquals(new BCryptPasswordEncoder(12).encode("1234"), userDB.getPassword());
        assertEquals(Provider.BASIC, userDB.getProvider());
    }

    @Test
    void shouldNotRegisterBasicUser_UserExist() {

        //given
        var user = new User();
        user.setId(1L);
        user.setName("Name");
        user.setEmail("user@mail.com");
        user.setPassword("1234");
        user.setProvider(Provider.BASIC);
        repository.save(user);

        var userDto = new UserRegistrationDto();
        userDto.setName("Name");
        userDto.setEmail("user@mail.com");
        userDto.setPassword("1234");

        //when
        //then
        assertThrows(UserRegistrationException.class, () -> userService.registerBasic(userDto));
    }

    @Test
    void shouldFindUser_getEmail() {
        //given
        var user = new User();
        user.setId(1L);
        user.setName("Name");
        user.setEmail("user@mail.com");
        user.setPassword("1234");
        user.setProvider(Provider.BASIC);
        repository.save(user);
        //when
        User userDB = userService.findByEmail("user@mail.com");
        //then
        assertEquals(user.getName(), userDB.getName());
        assertEquals(user.getEmail(), userDB.getEmail());
        assertEquals(user.getPassword(), userDB.getPassword());
        assertEquals(user.getProvider(), userDB.getProvider());
    }
}