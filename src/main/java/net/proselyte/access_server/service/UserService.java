package net.proselyte.access_server.service;

import net.proselyte.access_server.dto.access_server.UserRegistrationDto;
import net.proselyte.access_server.entity.access_server.User;
import net.proselyte.access_server.enumeration.Provider;
import net.proselyte.access_server.exception.UserRegistrationException;
import net.proselyte.access_server.repository.access_server.UserRepository;
import net.proselyte.access_server.security.oauth2.OAuth2UserImpl;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository repository;

    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public boolean registerOAuth2(OAuth2UserImpl oAuth2User) {
        if (!repository.existsByEmail(oAuth2User.getEmail())) {
            var newUser = new User();
            newUser.setName(oAuth2User.getName());
            newUser.setEmail(oAuth2User.getEmail());
            newUser.setPassword("OAuth2");
            newUser.setProvider(Provider.GOOGLE);
            repository.save(newUser);
            return true;
        }else {
            return false;
        }
    }

    public void registerBasic(UserRegistrationDto user) throws UserRegistrationException {
        if (!repository.existsByEmail(user.getEmail())) {
            var newUser = new User();
            newUser.setName(user.getName());
            newUser.setEmail(user.getEmail());
            newUser.setPassword(new BCryptPasswordEncoder(12).encode(user.getPassword()));
            newUser.setProvider(Provider.BASIC);
            repository.save(newUser);
        } else {
            throw new UserRegistrationException("User already exists");
        }
    }

    public User findByEmail(String email) {
        return repository.findByEmail(email);
    }
}
