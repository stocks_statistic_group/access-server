package net.proselyte.access_server.entity.access_server;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.proselyte.access_server.enumeration.Provider;
import javax.persistence.*;

@Entity
@Table(name = "users")
@NoArgsConstructor
@Getter
@Setter
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false, unique = true)
    private String email;
    @Column(nullable = false)
    private String password;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Provider provider;
}
