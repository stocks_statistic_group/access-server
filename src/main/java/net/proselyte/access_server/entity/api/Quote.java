package net.proselyte.access_server.entity.api;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "quotes")
@Getter
@Setter
@NoArgsConstructor
public class Quote {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "symbol_code")
    private String symbolCode;
    @Column(name = "previous_volume")
    private BigDecimal previousVolume;
    @Column(name = "volume")
    private BigDecimal volume;
    @Column(name = "latest_price")
    private BigDecimal latestPrice;

    @ManyToOne
    @JoinColumn(name = "symbol_id")
    private Symbol symbolObject;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var quote = (Quote) o;
        return Objects.equals(symbolCode, quote.symbolCode) &&
                Objects.requireNonNullElse(previousVolume, BigDecimal.ZERO)
                        .compareTo(Objects.requireNonNullElse(quote.previousVolume, BigDecimal.ZERO)) == 0 &&
                Objects.requireNonNullElse(volume, BigDecimal.ZERO)
                        .compareTo(Objects.requireNonNullElse(quote.volume, BigDecimal.ZERO)) == 0 &&
                Objects.requireNonNullElse(latestPrice, BigDecimal.ZERO)
                        .compareTo(Objects.requireNonNullElse(quote.latestPrice, BigDecimal.ZERO)) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(symbolCode, previousVolume, volume, latestPrice);
    }
}
