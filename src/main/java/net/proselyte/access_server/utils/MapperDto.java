package net.proselyte.access_server.utils;

import net.proselyte.access_server.dto.api.QuoteDto;
import net.proselyte.access_server.dto.api.SymbolDto;
import net.proselyte.access_server.entity.api.Quote;
import net.proselyte.access_server.entity.api.Symbol;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import java.util.List;

@Mapper(componentModel = "spring")
public interface MapperDto {

    @BeanMapping(ignoreByDefault = true)
    @Mappings({
            @Mapping(target = "symbolCode", source = "symbol.symbolCode"),
            @Mapping(target = "name", source = "symbol.name"),
            @Mapping(target = "enabled", source = "symbol.enabled")
    })
    SymbolDto symbolToSymbolDto(Symbol symbol);

    List<SymbolDto> symbolToSymbolDto(List<Symbol> symbols);

    @BeanMapping(ignoreByDefault = true)
    @Mappings({
            @Mapping(target = "symbolCode", source = "quote.symbolCode"),
            @Mapping(target = "previousVolume", source = "quote.previousVolume"),
            @Mapping(target = "volume", source = "quote.volume"),
            @Mapping(target = "latestPrice", source = "quote.latestPrice")
    })
    QuoteDto quoteToQuoteDto(Quote quote);

    List<QuoteDto> quoteToQuoteDto(List<Quote> quotes);
}
