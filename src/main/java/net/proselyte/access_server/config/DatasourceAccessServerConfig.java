package net.proselyte.access_server.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "accessServerEntityManagerFactory",
        transactionManagerRef = "accessServerTransactionManager",
        basePackages = {"net.proselyte.access_server.repository.access_server"}
)
public class DatasourceAccessServerConfig {


    @Autowired
    JpaProperties jpaProperties;


    @Primary
    @Bean(name = "accessServerDataSource")
    @ConfigurationProperties(prefix = "spring.datasource-access-server")
    public DataSource customerDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Primary
    @Bean(name = "accessServerEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            Environment env,
            @Qualifier("accessServerDataSource") DataSource dataSource) {
        var em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan("net.proselyte.access_server.entity.access_server");
        var vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto",
                env.getProperty("spring.jpa.hibernate.ddl-auto-access-server"));
        properties.put("hibernate.dialect",
                env.getProperty("spring.jpa.properties.hibernate.dialect-access-server"));
        em.setJpaPropertyMap(properties);
        return em;
    }

    @Primary
    @Bean(name = "accessServerTransactionManager")
    public PlatformTransactionManager accessServerTransactionManager(
            @Qualifier("accessServerEntityManagerFactory") EntityManagerFactory accessServerEntityManagerFactory) {
        return new JpaTransactionManager(accessServerEntityManagerFactory);
    }
}
