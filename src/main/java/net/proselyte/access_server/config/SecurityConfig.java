package net.proselyte.access_server.config;

import net.proselyte.access_server.security.oauth2.OAuth2UserImpl;
import net.proselyte.access_server.security.oauth2.OAuth2UserServiceImpl;
import net.proselyte.access_server.service.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;
    private final OAuth2UserServiceImpl oauthUserService;
    private final UserService userService;

    public SecurityConfig(OAuth2UserServiceImpl oauthUserService,
                          UserService userService,
                          @Qualifier("userDetailsServiceImpl") UserDetailsService userDetailsService) {
        this.oauthUserService = oauthUserService;
        this.userService = userService;
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests(a -> a
                        .antMatchers("/",
                                "/registration",
                                "/login",
                                "/login-error",
                                "/error",
                                "/webjars/**", "/webjars/").permitAll()
                        .anyRequest().authenticated()
                )
                .exceptionHandling(e -> e
                        .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED))
                )
                .csrf(c -> c
                        .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
                )
                .logout(l -> l
                        .logoutRequestMatcher(new AntPathRequestMatcher("/logout", "POST"))
                        .invalidateHttpSession(true)
                        .clearAuthentication(true)
                        .deleteCookies("JSESSIONID")
                        .logoutSuccessUrl("/").permitAll()
                )
                .formLogin()
                .loginPage("/login")
                .failureUrl("/login-error")
                .and()
                .oauth2Login(o -> o
                        .loginPage("/login")
                        .userInfoEndpoint()
                        .userService(oauthUserService)
                        .and()
                        .failureUrl("/login-error")
                        .successHandler((request, response, authentication) -> {
                            var oauthUser = new OAuth2UserImpl((OAuth2User) authentication.getPrincipal());
                            userService.registerOAuth2(oauthUser);
                            response.sendRedirect("/");
                        })
                );
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(daoAuthenticationProvider());
    }

    @Bean
    protected PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(12);
    }

    @Bean
    protected DaoAuthenticationProvider daoAuthenticationProvider() {
        var daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        daoAuthenticationProvider.setUserDetailsService(userDetailsService);
        return daoAuthenticationProvider;
    }
}
