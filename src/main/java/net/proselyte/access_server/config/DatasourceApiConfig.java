package net.proselyte.access_server.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "apiEntityManagerFactory",
        transactionManagerRef = "apiTransactionManager",
        basePackages = {"net.proselyte.access_server.repository.api"}
)
public class DatasourceApiConfig {

    @Bean(name = "apiDataSource")
    @ConfigurationProperties(prefix = "spring.datasource-api")
    public DataSource dataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "apiEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean
    barEntityManagerFactory(
            Environment env,
            @Qualifier("apiDataSource") DataSource dataSource) {
        var em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan("net.proselyte.access_server.entity.api");
        var vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto",
                env.getProperty("spring.jpa.hibernate.ddl-auto-api"));
        properties.put("hibernate.dialect",
                env.getProperty("spring.jpa.properties.hibernate.dialect-api"));
        em.setJpaPropertyMap(properties);
        return em;
    }

    @Bean(name = "apiTransactionManager")
    public PlatformTransactionManager productTransactionManager(
            @Qualifier("apiEntityManagerFactory") EntityManagerFactory apiEntityManagerFactory) {
        return new JpaTransactionManager(apiEntityManagerFactory);
    }
}
