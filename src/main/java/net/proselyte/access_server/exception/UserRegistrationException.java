package net.proselyte.access_server.exception;

public class UserRegistrationException extends Exception {

    public UserRegistrationException(String message) {
        super(message);
    }
}
