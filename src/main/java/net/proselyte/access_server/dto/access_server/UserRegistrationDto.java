package net.proselyte.access_server.dto.access_server;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRegistrationDto {

    private String name;
    private String email;
    private String password;
}
