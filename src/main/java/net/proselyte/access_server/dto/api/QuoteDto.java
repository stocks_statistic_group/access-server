package net.proselyte.access_server.dto.api;

import lombok.Getter;
import lombok.Setter;
import java.math.BigDecimal;

@Getter
@Setter
public class QuoteDto {

    private String symbolCode;
    private BigDecimal previousVolume;
    private BigDecimal volume;
    private BigDecimal latestPrice;
}
