package net.proselyte.access_server.dto.api;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SymbolDto {

    private String symbolCode;
    private String name;
    private boolean isEnabled;
}
