package net.proselyte.access_server.controller;

import net.proselyte.access_server.dto.access_server.UserRegistrationDto;
import net.proselyte.access_server.entity.access_server.User;
import net.proselyte.access_server.exception.UserRegistrationException;
import net.proselyte.access_server.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import java.security.Principal;

@Controller
@RequestMapping("/")
public class AccessController {

    private final UserService userService;

    public AccessController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/")
    public String getIndex(Principal principal) {
        if (principal == null) {
            return "redirect:/login";
        } else {
            return "index";
        }
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "login";
    }

    @GetMapping("/registration")
    public String getRegistrationForm(final Model model) {
        var user = (User) model.getAttribute("user");
        if (user == null) {
            model.addAttribute("user", new User());
        }
        return "registration";
    }

    @PostMapping("/registration")
    public String register(@ModelAttribute UserRegistrationDto newUser, final Model model) {
        try {
            userService.registerBasic(newUser);
        } catch (UserRegistrationException e) {
            model.addAttribute("user", newUser);
            model.addAttribute("registrationError", true);
            model.addAttribute("errorMessage", e.getMessage());
            return "registration";
        }
        return "redirect:/login";
    }
}
