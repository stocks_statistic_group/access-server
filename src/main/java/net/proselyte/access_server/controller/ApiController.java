package net.proselyte.access_server.controller;

import net.proselyte.access_server.dto.api.QuoteDto;
import net.proselyte.access_server.dto.api.SymbolDto;
import net.proselyte.access_server.entity.api.Quote;
import net.proselyte.access_server.entity.api.Symbol;
import net.proselyte.access_server.repository.api.QuoteRepository;
import net.proselyte.access_server.repository.api.SymbolRepository;
import net.proselyte.access_server.utils.MapperDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ApiController {

    private final MapperDto mapperDto;
    private final QuoteRepository quoteRepository;
    private final SymbolRepository symbolRepository;

    public ApiController(SymbolRepository symbolRepository, QuoteRepository quoteRepository, MapperDto mapperDto) {
        this.symbolRepository = symbolRepository;
        this.quoteRepository = quoteRepository;
        this.mapperDto = mapperDto;
    }

    @GetMapping("/symbol/{symbolCode}")
    public ResponseEntity<SymbolDto> getSymbolBySymbolCode(@PathVariable String symbolCode) {
        Optional<Symbol> optionalSymbol = symbolRepository.findBySymbolCode(symbolCode);
        return optionalSymbol
                .map(symbol -> new ResponseEntity<>(mapperDto.symbolToSymbolDto(symbol), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @GetMapping("/symbols")
    public ResponseEntity<Object> getAllSymbols() {
        return new ResponseEntity<>(mapperDto.symbolToSymbolDto(symbolRepository.findAll()), HttpStatus.OK);
    }

    @GetMapping("/last-quote/{symbolCode}")
    public ResponseEntity<QuoteDto> getLastQuoteBySymbolCode(@PathVariable String symbolCode) {
        Optional<Quote> optionalQuote = quoteRepository.findFirstBySymbolCodeOrderByIdDesc(symbolCode);
        return optionalQuote
                .map(quote -> new ResponseEntity<>(mapperDto.quoteToQuoteDto(quote), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
