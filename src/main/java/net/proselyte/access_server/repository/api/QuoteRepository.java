package net.proselyte.access_server.repository.api;

import net.proselyte.access_server.entity.api.Quote;
import org.springframework.data.repository.CrudRepository;
import java.util.Optional;

public interface QuoteRepository extends CrudRepository<Quote, Long> {

    Optional<Quote> findFirstBySymbolCodeOrderByIdDesc(String symbolCode);
}

