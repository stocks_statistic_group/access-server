package net.proselyte.access_server.repository.api;

import net.proselyte.access_server.entity.api.Symbol;
import org.springframework.data.repository.CrudRepository;
import java.util.List;
import java.util.Optional;

public interface SymbolRepository extends CrudRepository<Symbol, Long> {

    List<Symbol> findAll();

    Optional<Symbol> findBySymbolCode(String symbolCode);

    boolean existsBySymbolCode(String symbolCode);
}

