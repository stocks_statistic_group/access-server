package net.proselyte.access_server.repository.access_server;

import net.proselyte.access_server.entity.access_server.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

    boolean existsByEmail(String email);

    User findByEmail(String email);
}
