FROM openjdk:17

RUN mkdir -p /usr/local/src
COPY . /usr/local/src
WORKDIR /usr/local/src
RUN ./gradlew build
ARG JAR_FILE=/usr/local/src/build/libs/*SNAPSHOT.jar
RUN mkdir -p /usr/local/access_server
RUN cp ${JAR_FILE} /usr/local/access_server/app.jar
RUN rm -r /usr/local/src
WORKDIR /usr/local/access_server

ARG GOOGLE_CLIENT_SECRET
ARG GOOGLE_CLIENT_ID
ARG DATASOURCE_ACCESS_SERVER_JDBC_URL
ARG DATASOURCE_ACCESS_SERVER_USERNAME
ARG DATASOURCE_ACCESS_SERVER_PASSWORD
ARG DATASOURCE_API_JDBC_URL
ARG DATASOURCE_API_USERNAME
ARG DATASOURCE_API_PASSWORD

ENV GOOGLE_CLIENT_SECRET=${GOOGLE_CLIENT_SECRET}
ENV GOOGLE_CLIENT_ID=${GOOGLE_CLIENT_ID}
ENV DATASOURCE_ACCESS_SERVER_JDBC_URL=${DATASOURCE_ACCESS_SERVER_JDBC_URL}
ENV DATASOURCE_ACCESS_SERVER_USERNAME=${DATASOURCE_ACCESS_SERVER_USERNAME}
ENV DATASOURCE_ACCESS_SERVER_PASSWORD=${DATASOURCE_ACCESS_SERVER_PASSWORD}
ENV DATASOURCE_API_JDBC_URL=${DATASOURCE_API_JDBC_URL}
ENV DATASOURCE_API_USERNAME=${DATASOURCE_API_USERNAME}
ENV DATASOURCE_API_PASSWORD=${DATASOURCE_API_PASSWORD}

EXPOSE 8080
ENTRYPOINT java -jar app.jar --google.clientSecret=${GOOGLE_CLIENT_SECRET} \
 --google.clientId=${GOOGLE_CLIENT_ID} \
 --datasource.access-server.jdbc-url=${DATASOURCE_ACCESS_SERVER_JDBC_URL} \
 --datasource.access-server.username=${DATASOURCE_ACCESS_SERVER_USERNAME} \
 --datasource.access-server.password=${DATASOURCE_ACCESS_SERVER_PASSWORD} \
 --datasource.api.jdbc-url=${DATASOURCE_API_JDBC_URL} \
 --datasource.api.username=${DATASOURCE_API_USERNAME} \
 --datasource.api.password=${DATASOURCE_API_PASSWORD}
